from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import MaxPooling2D

import matplotlib.pyplot as plt

# Initialising the CNN
classifier = Sequential()

# First Convolutional Layer 
# Number of Feature Detectors = 32
# Feature Detector Dimensions = 3 x 3
# Image Dimensions = 64 x 64 x 3 (3 => RGB)
# Activation Function = Rectified Linear Unit (ReLU)
# Uses Max Pooling
# Stride Dimensions = 2 x 2
classifier.add(Conv2D(32, (3, 3), input_shape = (64, 64, 3), activation = 'relu'))
classifier.add(MaxPooling2D(pool_size = (2, 2)))

# Second Convolutional Layer
classifier.add(Conv2D(32, (3, 3), activation = 'relu'))
classifier.add(MaxPooling2D(pool_size = (2, 2)))

# The Flattening stage
classifier.add(Flatten())

# Full connection Layer
# Number of units = 128 (Based on Intuition)
# Activation Function = Rectified Linear Unit (ReLU)
classifier.add(Dense(units = 128, activation = 'relu'))

# Output Layer
# Number of units = 1 (Assuming binary classification rather than multiclass classification.)
# Activation Function = Sigmoid (Since we are using Binary Classification)
classifier.add(Dense(units = 1, activation = 'sigmoid'))

# Compiling the CNN
# Optimization Algorithm = ADAM (To gain the benefits of both Gradient Descent with Momentum + RMSProp)
# Loss Function = Binary Cross Entrophy
# Performance Metric = Accuracy
classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

# Fitting the CNN to the images

from keras.preprocessing.image import ImageDataGenerator

# Generate Training images through Image Augmentation Techniques
# Rescale = 1/255 to scale the pixle values (0 - 255) into (0, 1)
# Shear Range = 0.2 (The range of random shear)
# Zoom Range = 0.2 (The range of random zoom)
# Horizontal Flip = True (Generates random samples by flipping as well)
train_datagen = ImageDataGenerator(rescale = 1./255,
                                   shear_range = 0.2,
                                   zoom_range = 0.2,
                                   horizontal_flip = True)

test_datagen = ImageDataGenerator(rescale = 1./255)

training_set = train_datagen.flow_from_directory('../dataset/training_set',
                                                 target_size = (64, 64),
                                                 batch_size = 32,
                                                 class_mode = 'binary')

test_set = test_datagen.flow_from_directory('../dataset/test_set',
                                            target_size = (64, 64),
                                            batch_size = 32,
                                            class_mode = 'binary')

history = classifier.fit_generator(training_set,
                         steps_per_epoch = 1000,
                         epochs = 25,
                         validation_data = test_set,
                         validation_steps = 250)

# list all data in history
print(history.history.keys())

# summarize history for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc = 'upper left')
plt.show()

# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc = 'upper left')
plt.show()
